Mirror of [OPAM](http://github.com/OCamlPro/opam) repository

Archives are also copied, so good when some archive sites are down.
The update is periodically, about once a week basis.

Add `ocaml_tan` mirror to your opam with the lowest priority:

    opam remote add -p 0 ocaml_tan https://bitbucket.org/ocaml_tan/opam-repository.git
    opam remote priority default 10

Something goes wrong with `default`, raise the priority of `ocaml_tan`:

    opam remote priority ocaml_tan 20

And enjoy the mirrored archives:

    opam install ...

Do not forget to lower the priority back when `default` is working:

    opam remote priority ocaml_tan 0
